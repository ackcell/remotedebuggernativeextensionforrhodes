# remote debugger native extension for Rhodes framework on iOS

This extension was inspired by https://groups.google.com/forum/#!msg/rhomobile/zQQw_IT-XD0/wb8IIYLUuuQJ .


## Quick Start
###1. copy the extension into extension dir of your rhodes app
###2. edit your build.yml 

    iphone:
    extensions:
    - rdebug

###3. build & run on Simulator
###4. open http://localhost:9999 in Safari

## REMARK
* remote debugger native extension works on iOS simulator 5.x only.
* the extenson calls private API

## References
### https://groups.google.com/forum/#!msg/rhomobile/zQQw_IT-XD0/wb8IIYLUuuQJ
### http://atnan.com/blog/2011/11/17/enabling-remote-debugging-via-private-apis-in-mobile-safari/
