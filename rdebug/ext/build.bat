
if "%RHO_PLATFORM%" == "android" (

cd rdebug\platform\android
rake --trace

)

if "%RHO_PLATFORM%" == "iphone" (

cd rdebug\platform\phone
rake --trace

)

if "%RHO_PLATFORM%" == "wm" (

cd rdebug\platform\wm
rake --trace

)

if "%RHO_PLATFORM%" == "win32" (

cd rdebug\platform\wm
rake --trace

)

if "%RHO_PLATFORM%" == "bb" (

cd rdebug\platform\bb
rake --trace

)

