
#import "Rdebug.h"

#include "ruby/ext/rho/rhoruby.h"


void rdebug_check_os(void){
#if TARGET_IPHONE_SIMULATOR
    if ([NSClassFromString(@"WebView") respondsToSelector:@selector(_enableRemoteInspector)]) {
        NSLog(@"enableRemoteInspector");
        [NSClassFromString(@"WebView") performSelector:@selector(_enableRemoteInspector)];
    }
#endif
    
}
